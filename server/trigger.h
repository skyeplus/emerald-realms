#ifndef _TRIGGER_H_3668135E_B25D_4F02_8D73_E23F0430F3BB_
#define _TRIGGER_H_3668135E_B25D_4F02_8D73_E23F0430F3BB_

namespace realm {

class Object;

class Trigger {
 public:
  enum Type {
    kVerbal,
    kOnHit,
    kOnBeingHit,
    kOnSleep,
    kOnEvade,
    kOnSpellcast,
    kOnMagicDamage, // Being hit by magic
    kOnPositiveSpell, // Being affected by positive spell
    kOnNegativeSpell, // Being affected by negative spell
    kOnHeal,
    kOnMortalStrike,
    kCustom
  };

  Trigger(Type type);
  virtual ~Trigger();

  void Init(Object* object);

  virtual bool Check(const Object* target) const;

  virtual Object* object();
  virtual const Object* object() const;

  Type type() const {
    return type_;
  }

  float probability() const {
    return probability_;
  }
  void probability(float probability) {
    probability_ = probability;
  }
 private:
  Type type_;
  float probability_; // Probability
  Object* object_;
};

} // realm

#endif // _TRIGGER_H_3668135E_B25D_4F02_8D73_E23F0430F3BB_
