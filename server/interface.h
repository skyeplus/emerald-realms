// $Sourceworks License Boilerplate$
#ifndef _INTERFACE_H_11E24F4B_911F_4CFE_831B_03727192CA9E_
#define _INTERFACE_H_11E24F4B_911F_4CFE_831B_03727192CA9E_

namespace realm {

class Object;

class Interface {
 public:
  enum Type {
    StaticObject = 0,
    InteractiveObject,
    ContainerObject,
    WeaponObject,
    WearableObject,
    UsableObject,
    FoodObject,
    Unit
  };

  Interface(Object* parent_object, Type type);
  virtual ~Interface();

  void Init(); //

  std::string interface_name() const {
    return name_;
  }
  Type interface_type() const {
    return type_;
  }
  Object* object() {
    return object_;
  }
  const Object* object() const {
    return object_;
  }
 
 private:
  std::string name_;
  Type type_;
  Object* object_;
};

} // realm

#endif // _INTERFACE_H_11E24F4B_911F_4CFE_831B_03727192CA9E_
