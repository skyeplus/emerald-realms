#include "storage.h"

Storage::Storage() {
}

Storage::~Storage() {
}

// IniFileStorage

IniFileStorage::IniFileStorage(std::istream& ist, std::ostream& ost) :
  ist_(ist),
  ost_(ost) {
}

IniFileStorage::~IniFileStorage() {
}

void IniFileStorage::StartObject(const std::string& object_name) {
  ost_ << "[" << object_name << "]" << std::endl
}

void IniFileStorage::EndObject(const std::string& object_name) {

}

void IniFileStorage::WriteProperty(const std::string& name) {

}

void IniFileStorage::WriteProperty(const std::string& name, const std::string& value) {

}

void IniFileStorage::ReadProperty(const std::string& name, std::string& value) {

}

std::istream& IniFileStorage::is() {
  return ist_;
}

std::ostream& IniFileStorage::os() {
  return ost_;
}
