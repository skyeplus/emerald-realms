#ifndef _EM_OBJECT_H_
#define _EM_OBJECT_H_

#include "interface.h"
#include "triggers.h"

namespace realm {

class Object {
 public:
 	void* interface(InterfaceType interface);
 	const void* interface(InterfaceType interface) const;
  std::list<InterfaceType> interfaces() const;

  bool is_static() const;
  bool is_interactive() const;
  bool is_container() const;
  bool is_weapon() const;
  bool is_wearable() const;
  bool is_usable() const;
  bool is_food() const;
  bool is_unit() const;

  std::list<std::shared_ptr<Trigger>> triggers();

 private:
  std::string descr_;
  std::string full_descr_;
  
  bool movable_;
  bool pickable_;

  float weight_;
  float size_; // how much space object occupies. TODO
  
  std::vector<Interface*> interfaces_;
};

} // namespace realm

#endif // _EM_OBJECT_H_
