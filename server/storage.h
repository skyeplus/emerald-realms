#ifndef _STORAGE_H_AF59098C_0345_490A_95BF_58F321C8EBCA_
#define _STORAGE_H_AF59098C_0345_490A_95BF_58F321C8EBCA_

#include <string>
#include <iostream>

class Storage {
 public:
  enum Type {
    kFileJson,
    kFileIni,
    kDatabase
  };
  Storage();
  ~Storage();
  
  virtual void StartObject(const std::string& object_name) = 0;
  virtual void EndObject(const std::string& object_name) = 0;
  virtual void StartSequence(const std::string& object_name) = 0;
  virtual void EndSequence(const std::string& object_name) = 0;
  virtual void WriteProperty(const std::string& name) = 0;
  virtual void WriteProperty(const std::string& name, const std::string& value) = 0;
  virtual void ReadProperty(const std::string& name, std::string& value) = 0;

  virtual std::istream& is() = 0;
  virtual std::ostream& os() = 0;
};

class IniFileStorage : public Storage {
 public:
  IniFileStorage(std::istream& ist, std::ostream& ost);
  ~IniFileStorage();

  std::istream& istream() {
    return ist_;
  }
  std::ostream& ostream() {
    return ost_;
  }
  
  virtual void StartObject(const std::string& object_name) = 0;
  virtual void EndObject(const std::string& object_name) = 0;
  virtual void WriteProperty(const std::string& name) = 0;
  virtual void WriteProperty(const std::string& name, const std::string& value) = 0;
  virtual void ReadProperty(const std::string& name, std::string& value) = 0;

 private:
  std::istream& ist_;
  std::ostream& ost_;
};

#endif // _STORAGE_H_AF59098C_0345_490A_95BF_58F321C8EBCA_
