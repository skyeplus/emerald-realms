#ifndef _SERIALIZER_H_7DEC0320_A08A_4CE0_83C0_0856023F2D31_
#define _SERIALIZER_H_7DEC0320_A08A_4CE0_83C0_0856023F2D31_

class Storage;

class Serializer {
 public:
  ~virtual Serializer();

  virtual bool Save(Storage* storage);
  virtual bool Load(Storage* storage);
};

#endif // _SERIALIZER_H_7DEC0320_A08A_4CE0_83C0_0856023F2D31_
