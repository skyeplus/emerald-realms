#ifndef _YAMLSTORAGE_H_1B990CB9_9ADE_4F90_939C_6687090C13B7_
#define _YAMLSTORAGE_H_1B990CB9_9ADE_4F90_939C_6687090C13B7_

#include "storage.h"

class YamlFileStorage : public Storage {
 public:
  YamlFileStorage(std::istream& ist, std::ostream& ost);
  ~YamlFileStorage();

  
  virtual void StartObject(const std::string& object_name);
  virtual void EndObject(const std::string& object_name);
  virtual void WriteProperty(const std::string& name);
  virtual void WriteProperty(const std::string& name, const std::string& value);
  virtual void ReadProperty(const std::string& name, std::string& value);

 private:
  
  std::istream& ist_;
  std::ostream& ost_;
};

#endif // _YAMLSTORAGE_H_1B990CB9_9ADE_4F90_939C_6687090C13B7_
