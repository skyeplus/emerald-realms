#include "yamlstorage.h"


YamlFileStorage::YamlFileStorage(std::istream& ist, std::ostream& ost) :
  ist_(ist),
  ost_(ost)
  {
  
}

YamlFileStorage::~YamlFileStorage();


void YamlFileStorage::StartObject(const std::string& object_name) {

}

void YamlFileStorage::EndObject(const std::string& object_name) {

}

void YamlFileStorage::WriteProperty(const std::string& name) {

}

void YamlFileStorage::WriteProperty(const std::string& name, const std::string& value) {

}

void YamlFileStorage::ReadProperty(const std::string& name, std::string& value) {

}
