#include "trigger.h"

namespace realm {

Trigger::Trigger(Trigger::Type type) :
  type_(type),
  probability_(0.0f),
  object_(nullptr) {
}

Trigger::~Trigger() {
}

void Trigger::Init(Object* object) {
  object_ = object;
}

bool Trigger::Check(const Object* target) const {
  return false;
}

Object* Trigger::object() {
  return object_;
}

const Object* Trigger::object() const {
  return object_;
}

} // namespace realm
