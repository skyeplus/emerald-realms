#ifndef _EM_OBJECT_H_
#define _EM_OBJECT_H_

namespace realm {

enum InterfaceType {
	StaticObject,
	InteractiveObject,
	Unit,

};

class Object {
 public:
 	void* GetInterface(InterfaceType interface);
};

} // namespace realm

#endif // _EM_OBJECT_H_