SERVER=esvc
CLIENT=em

CXX:=g++
INCLUDE_FLAGS=-I. 
LIB_FLAGS=

CXXFLAGS=-Wall -g -Og -std=c++14 $(INCLUDE_FLAGS) $(LIB_FLAGS)

SERVER_SRCS=$(wildcard server/*.cpp)
SERVER_OBJS=$(SERVER_SRCS:%.cpp=%.o)

CLIENT_SRCS=$(wildcard client/*.cpp)
CLIENT_OBJS=$(CLIENT_SRCS:%.cpp=%.o)

TEST_APP=emt
TEST_APP_OBJS=em_test.o

TARGETS=$(SERVER) $(CLIENT) $(TEST_APP)

all: $(TARGETS)

$(SERVER): $(SERVER_OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(CLIENT): $(CLIENT_OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(TEST_APP): $(TEST_APP_OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

server/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

client/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	@rm $(TARGET) $(SERVER_OBJS) $(CLIENT_OBJS)
